# State Pattern UI actors -- LabVIEW

![gif](./to_show/recording.gif)

This is my understanding of what Tom describes in his video: State of Grace - The State Pattern in LabVIEW, @ ~41 mins. https://youtu.be/HewNBC4TjKs?t=2482

It took me awhile to get this example working and I tried to keep it as simple as possible (no nested actors for the UI panel), but it should be trivial to add.

Please open PRs on a new branch if you see problems with my design. Still cutting my teeth on labVIEW, but this framework (despite the learning curve) and add-ons (listed below) make development bearable.

addons:
- Events for UI Actor Indicators: https://forums.ni.com/t5/Actor-Framework-Documents/Events-for-UI-Actor-Indicators/ta-p/3869260
    - A template saving a bunch of time writing boiler plate for UI actors
- NI State Pattern Actor: JKI's VI package manager
- NI GOOP Development Suite: JKI's VI package manager

## A quick taste:

### Class Hierarchy
![class_structure](./to_show/class_hierarchy.png)

#### Actor Core -- Panel (parent ui) 
The child actors do NOT implement actor core
![Panel](./to_show/panel_actor_core.png)

#### Panel private data
References to controls are stored in Panel actor's private data
![privates](./to_show/panel_private_data.png)

#### go to x -- Panel method
![goto](./to_show/go_to_x_change_state.png)

#### substitute actor overrride -- Panel Method
The child actors do NOT implement actor core
![sub](./to_show/substitute_actor_override.png)

#### x entry -- Panel X method
![sub](./to_show/x_entry.png)